package main

import (
	"fmt"
	"log"
	"reflect"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

var mySigningKey = "JhbGciOiJIUzI1N0eXAiOiJKV1QiLC"

func main() {
	createdToken, err := ExampleNew([]byte(mySigningKey), 12, "12agustus")
	if err != nil {
		fmt.Println("Creating token failed")
	}

	log.Println(createdToken)

	fmt.Println("=========================================================")
	tokens := "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZF91c2VyIjo1NDYsImVtYWlsIjoiQWNobWFkc3lhcmlwcHVkaW5udXJAZ21haWwuY29tIiwiaWF0IjoxNTU0NjIyNjc0LCJleHAiOjE1ODYxODI2NzR9.kUW96sGS2IearDd6AxCz2vIwhx2J6NaVWogePDajJ3U"
	ExampleParse(tokens, mySigningKey)
}

func ExampleNew(mySigningKey []byte, userId uint, password string) (string, error) {
	// Create the token
	token := jwt.New(jwt.SigningMethodHS256)
	claims := make(jwt.MapClaims)
	// Set some claims

	// layout := "2006-01-02T15:04:05+0700"
	claims["userId"] = userId
	claims["password"] = password
	claims["exp"] = time.Now().Add(time.Hour * 730).Local()
	token.Claims = claims
	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString(mySigningKey)
	return tokenString, err
}

func ExampleParse(myToken string, myKey string) {
	token, err := jwt.Parse(myToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(myKey), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		log.Println(" => ", claims)
		log.Println(" => ", claims["iat"])
		log.Println(" => ", reflect.TypeOf(claims["iat"]))
	} else {
		log.Println(err)
	}
}
