package main

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

// Activities table SQL :
// id          bigint(20)  AUTO_INCREMENT
// username    varchar(50)
// created_on  timestamp
// action      char(1)
// description varchar(300)
// visibility  char(1)

type Activities struct {
	Id          int    `sql:"AUTO_INCREMENT"`
	Username    string `sql:"varchar(50);unique"`
	Created_On  time.Time
	Action      string `sql:"type:char(1)"`
	Description string `sql:"type:varchar(300)"`
	Visibility  string `sql:"type:char(1)"`
}

func main() {

	dbConn, err := gorm.Open("mysql", "root:12agustus@tcp(localhost:3306)/syndb_2?charset=utf8&parseTime=true")

	if err != nil {
		fmt.Println(err)
	}

	// init
	dbConn.DB()
	dbConn.DB().Ping()
	dbConn.DB().SetMaxIdleConns(10)
	dbConn.DB().SetMaxOpenConns(100)

	activity := Activities{} // a record

	// get first record
	dbConn.First(&activity)

	fmt.Println(activity)

	fmt.Println("------------------------------")
	// get all records
	activities := []Activities{} // a slice

	//dbConn.Find(&activities)
	//fmt.Println(activities)

	// get records from record offset 1 to 10
	// useful for pagination purpose! - just need to calculate the offset and limit dynamically

	dbConn.Limit(10).Order("id asc").Find(&activities).Offset(1).Order("id asc").Find(&activities)

	for _, v := range activities {
		fmt.Println("Id : ", v.Id)
		fmt.Println("Username : ", v.Username)
		fmt.Println("Created On : ", v.Created_On)
		fmt.Println("Action : ", v.Action)
		fmt.Println("Desription : ", v.Description)
		fmt.Println("Visibility : ", v.Visibility)
	}

}
