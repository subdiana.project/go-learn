package get

import (
	"net/http"
)

type (
	getMethod struct {
		UserID uint
		ID     uint
		Title  string
		Body   string
	}
)

func MetGet(url string) string {
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}

	// fmt.Println(resp)
	// body, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	panic(err)
	// }

	// body.StatusCode
	return resp.Status

	// parsing Struct
	// var GetMethod []getMethod
	// if err := json.Unmarshal(body, &GetMethod); err != nil {
	// 	panic(err)
	// }

	// fmt.Println(len(GetMethod))
	// for i := 0; i < len(GetMethod); i++ {
	// 	fmt.Println(GetMethod)
	// }
}
