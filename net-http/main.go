package main

import (
	"fmt"
	"log"
	"time"

	. "./get"
	// . "./post"
)

func main() {
	start := time.Now()

	// MetPost("https://jsonplaceholder.typicode.com/posts")
	// fmt.Println("=======================================================================")
	// MetGet("https://jsonplaceholder.typicode.com/comments?postId=1")

	for i := 1; i <= 2000; i++ {
		log.Println("Hit : ", i, " => ", MetGet("http://ved.carsworld.id:2332/api/v2/Booking/153/13"))
		el := time.Since(start)
		fmt.Printf("Time Execute : %s", el)
		fmt.Println("")
		fmt.Println("")
	}
	el := time.Since(start)
	log.Printf("Time Execute : %s", el)
}
