package post

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type (
	postMethod struct {
		UserID uint
		ID     uint
		Title  string
		Body   string
	}
	post struct {
		UserID uint
		Title  string
		Body   string
	}
)

func MetPost(uri string) {

	var data post
	data.UserID = 1
	data.Title = "Post For Golang"
	data.Body = "Go Post net / http"

	// fmt.Println(data)
	byte := new(bytes.Buffer)
	json.NewEncoder(byte).Encode(data)
	resp, err := http.Post(uri, "application/json; charset=utf-8", byte)
	// resp, err := http.PostForm(uri, url.Values{"title": {"Post For Golang"}, "body": {"Go Post net / http"}, "userId": {"1"}})
	if err != nil {
		panic(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	// fmt.Println(string(body))

	// parsing Struct
	var post postMethod
	if err := json.Unmarshal(body, &post); err != nil {
		panic(err)
	}

	fmt.Println(post)
}
