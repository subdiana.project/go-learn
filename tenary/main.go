package main

import "fmt"

func main() {

	result := 10
	logic := 5

	// result = (logic > 10) ? 1 : 0

	a := (map[bool]int{true: result, false: logic})[result < logic]
	fmt.Println(a)
}
