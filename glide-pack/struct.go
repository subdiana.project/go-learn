package main

import "time"

type (
	TbUser struct {
		ID       uint   `json:"id"`
		Username string `json:"username"`
		Password string `json:"password"`
	}

	Users struct {
		ID       uint   `json:"id"`
		Username string `json:"username"`
		Password string `json:"password"`
	}

	BaySync struct {
		IDBayauct   uint   `json:"idBayauct"`
		VehicleCode string `json:"vehicleCode"`
		Merk        string `json:"merk"`
	}

	Carfix struct {
		VehicleCode   string    `json:"VehicleCode"`
		KodeBrand     string    `json:"KodeBrand"`
		BrandMobil    string    `json:"BrandMobil"`
		KodeModel     string    `json:"KodeModel"`
		ModelMobil    string    `json:"ModelMobil"`
		TipeMobil     string    `json:"TipeMobil"`
		VehicleTypeID string    `json:"VehicleTypeId"`
		TahunMobil    string    `json:"TahunMobil"`
		KategoriMobil string    `json:"KategoriMobil"`
		GradeMobil    string    `json:"GradeMobil"`
		MesinMobile   string    `json:"MesinMobile"`
		VehicleID     string    `json:"VehicleId"`
		TglUpload     time.Time `json:"TglUpload"`
		Area          string    `json:"Area"`
	}

	Kend struct {
		VehicleCode string `json:"VehicleCode"`
		KodeBrand   string `json:"KodeBrand"`
		Result      []Carfix
	}
)

// Set Struct To Table
func (Carfix) TableName() string {
	return "tb_carfix"
}
