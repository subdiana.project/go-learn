package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *gorm.DB

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.GET("/Data/Carfix", GetCarfix)
	r.Run() // listen and serve on 0.0.0.0:8080
}

func init() {
	//open a db connection
	var err error
	db, err = gorm.Open("mysql", "root:12agustus@/syncdb?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}

}

func GetCarfix(res *gin.Context) {
	var result []Carfix
	// var users []Users

	db.Find(&result)
	if len(result) <= 0 {
		res.JSON(http.StatusNotFound, gin.H{
			"code":    http.StatusNotFound,
			"message": "Data Not Found",
		})
		return
	}

	var kend []Kend
	for _, i := range result {
		kend = append(kend, Kend{
			VehicleCode: i.VehicleCode,
			KodeBrand:   i.KodeBrand,
		})
	}

	res.JSON(http.StatusOK, gin.H{
		"code":     http.StatusOK,
		"message":  "OK",
		"rawCount": len(result),
		"result":   result,
		// "vehicleCode": kend,
	})

	// fmt.Println(len(kend))
	// for _, item := range user {
	// 	result = append(result, Carfix{ID: item.ID, Username: item.Username, Password: item.Password})
}
