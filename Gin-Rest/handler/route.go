package handler

import (
	"github.com/gin-gonic/gin"
)

func HandleIndex() gin.HandlerFunc {
	return func(res *gin.Context) {

		res.JSON(200, gin.H{
			"code":    200,
			"message": "Welcome To Carsworld API",
		})
	}
}

func HandelData() gin.HandlerFunc {
	return func(res *gin.Context) {
		res.JSON(201, gin.H{
			"code":    201,
			"message": "Created",
		})
	}
}

func HandleReq() gin.HandlerFunc {
	return func(res *gin.Context) {
		res.JSON(202, gin.H{
			"code":    202,
			"message": "Accepted",
		})
	}
}

func HandleGetData() gin.HandlerFunc {
	return func(res *gin.Context) {
		res.JSON(202, gin.H{
			"code":    202,
			"message": "Accepted",
		})
	}
}
