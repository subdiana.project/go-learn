package main

import (
	handeler "./handler"
	"github.com/gin-gonic/gin"
)

func main() {
	route := gin.Default()

	route.GET("/", handeler.HandleIndex())
	route.GET("/Data", handeler.HandelData())
	route.GET("/Request", handeler.HandleReq())
	route.GET("/Get/Data", handeler.HandleGetData())

	route.Run(":7025") // listen default serve on 0.0.0.0:8080

}
