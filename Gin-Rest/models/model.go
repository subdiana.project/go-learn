package models

import "github.com/gin-gonic/gin"

func GetData() gin.HandlerFunc {
	return func(res *gin.Context) {
		res.JSON(201, gin.H{
			"code":    201,
			"message": "Created",
		})
	}
}
