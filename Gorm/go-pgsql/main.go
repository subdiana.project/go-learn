package main

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func (Product) TableName() string {
	return "tb_product"
}

func main() {
	db, err := gorm.Open("postgres", "host=localhost port=5432 user=go-pgsql dbname=go-pgsql password=go-pgsql")
	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	// db.AutoMigrate(&Product{})

	// Create
	db.Create(&Product{Code: "M1212", Price: 1000})

	fmt.Println(Product{})

}
