package main

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	db, err := gorm.Open("mysql", "root:12agustus@/go_learn?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}

	er := db.DB().Ping()
	fmt.Println(er)

	defer db.Close()

	db.DropTableIfExists(&Owner{}, &Book{}, &Author{})
	db.CreateTable(&Owner{}, &Book{}, &Author{})

	// own := Owner{
	// 	FirstName: "iyan",
	// 	LastName:  "subdiana",
	// }
	//
	// db.Create(&own)
	// own.FirstName = "yayan"

	// db.Save(&own)
	// var ow Owner
	// db.Debug().First(&ow)
	// fmt.Println(ow)
	//
	// // delete Data
	// db.Delete(&own)

}

type Owner struct {
	gorm.Model
	FirstName string
	LastName  string
	Books     []Book
}

type Book struct {
	gorm.Model
	Name        string
	PublishDate time.Time
	OwnerID     uint     `sql:"index"`
	Authors     []Author `gorm:"many2many:books_authors"`
}

type Author struct {
	gorm.Model
	FirstName string
	LastName  string
}
